package com.andrognito.rxpatternlockview;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.rxpatternlockview.events.PatternLockCompleteEvent;
import com.andrognito.rxpatternlockview.events.PatternLockCompoundEvent;
import com.andrognito.rxpatternlockview.events.PatternLockProgressEvent;
import com.andrognito.rxpatternlockview.observables.PatternLockViewCompleteObservable;
import com.andrognito.rxpatternlockview.observables.PatternLockViewCompoundObservable;
import com.andrognito.rxpatternlockview.observables.PatternLockViewProgressObservable;

import io.reactivex.Observable;

import static com.andrognito.rxpatternlockview.utils.Preconditions.checkNotNull;

/**
 * Created by aritraroy on 27/03/17.
 */

public class RxPatternLockView {


    public static Observable<PatternLockCompoundEvent> patternChanges(PatternLockView patternLockView) {
        checkNotNull(patternLockView, "view == null");
        return new PatternLockViewCompoundObservable(patternLockView, false);
    }


    public static Observable<PatternLockCompoundEvent> patternChanges(PatternLockView patternLockView,
                                                                      boolean emitInitialValue) {
        checkNotNull(patternLockView, "view == null");
        return new PatternLockViewCompoundObservable(patternLockView, emitInitialValue);
    }


    public static Observable<PatternLockCompleteEvent> patternComplete(PatternLockView patternLockView) {
        checkNotNull(patternLockView, "view == null");
        return new PatternLockViewCompleteObservable(patternLockView, false);
    }


    public static Observable<PatternLockCompleteEvent> patternComplete(PatternLockView patternLockView,
                                                                       boolean emitInitialValues) {
        checkNotNull(patternLockView, "view == null");
        return new PatternLockViewCompleteObservable(patternLockView, emitInitialValues);
    }


    public static Observable<PatternLockProgressEvent> patternProgress(PatternLockView patternLockView) {
        checkNotNull(patternLockView, "view == null");
        return new PatternLockViewProgressObservable(patternLockView, false);
    }


    public static Observable<PatternLockProgressEvent> patternProgress(PatternLockView patternLockView,
                                                                       boolean emitInitialValues) {
        checkNotNull(patternLockView, "view == null");
        return new PatternLockViewProgressObservable(patternLockView, emitInitialValues);
    }
}
