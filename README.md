# PatternLockView
一个自定义屏幕图案解锁控件，该库使您可以轻松，快速地在应用程序中实现模式锁定机制。它非常易于使用，并且提供了许多自定义选项，可以更改此视图的功能和外观以满足您的需求。它还支持RxJava 2视图绑定，因此，如果您喜欢响应式编程（就像我一样），则可以在用户绘制模式时获得更新流。

# 概述
* 支持手势滑动解锁绘制以及回调核心功能
* 鸿蒙Touch事件获取的坐标值和原组件有些差异,使用时请注意

# 演示
<img src="screenshot/PatternLockView.gif" width="75%"/>

# 集成
"apiVersion": {
      "compatible": 5,
      "target": 5,
      "releaseType": "Beta1" //此处没有需要自己添加
    }
4.修改readme中集成方式

```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:patternlockview-core:1.0.0' 
implementation 'io.openharmony.tpc.thirdlib:patternlockview-reactive:1.0.0' 
implementation 'io.reactivex.rxjava2:rxjava:2.0.2'
```

# entry运行要求
   通过DevEco studio,并下载SDK
   将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）


# 示例

```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DependentLayout
          xmlns:ohos="http://schemas.huawei.com/res/ohos"
          xmlns:app="http://schemas.huawei.com/res/app"
          ohos:width="match_parent"
          ohos:height="match_parent"
          ohos:background_element="#000000">

      <DirectionalLayout
              ohos:orientation="vertical"
              ohos:height="match_content"
              ohos:width="match_parent">

          <Image
                  ohos:id="$+id:profile_image"
                  ohos:width="84vp"
                  ohos:height="84vp"
                  ohos:layout_alignment="center"
                  ohos:top_margin="104vp"
                  ohos:image_src="$media:img_no_avatar"/>

          <Text
                  ohos:id="$+id:profile_name"
                  ohos:width="match_content"
                  ohos:height="match_content"
                  ohos:layout_alignment="center"
                  ohos:top_margin="36vp"
                  ohos:text_alignment="center"
                  ohos:max_text_lines="1"
                  ohos:text="Welcome"
                  ohos:text_color="#fff"
                  ohos:text_size="34fp"/>
      </DirectionalLayout>
      <com.andrognito.patternlockview.PatternLockView
              ohos:align_parent_bottom="true"
              ohos:center_in_parent="true"
              ohos:id="$+id:patter_lock_view"
              ohos:width="280vp"
              ohos:height="280vp"
              ohos:bottom_margin="50vp"
              ohos:layout_alignment="center"
              ohos:top_margin="16vp"
              app:dotNormalSize="14vp"
              app:aspectRatio="width_bias"
              app:aspectRatioEnabled="true"
              app:dotAnimationDuration="150"
              app:correctStateColor="#008000"
              app:normalStateColor="#ffffff"
              app:dotCount="3"/>

  </DependentLayout>

 ```


```java

  /**
   * 屏幕解锁示例
   */
  public class MainAbilitySlice extends AbilitySlice {

      private PatternLockView mPatternLockView;

      private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
          @Override
          public void onStarted() {
              LogUtil.error(getClass().getName(), "Pattern drawing started");
          }

          @Override
          public void onProgress(List<PatternLockView.Dot> progressPattern) {
             LogUtil.error(getClass().getName(), "Pattern progress: " +
                      PatternLockUtils.patternToString(mPatternLockView, progressPattern));
          }

          @Override
          public void onComplete(List<PatternLockView.Dot> pattern) {
              LogUtil.error(getClass().getName(), "Pattern complete: " +
                      PatternLockUtils.patternToString(mPatternLockView, pattern));
          }

          @Override
          public void onCleared() {
              LogUtil.error(getClass().getName(), "Pattern has been cleared");
          }
      };

      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_main);
          mPatternLockView = (PatternLockView) findComponentById(ResourceTable.Id_patter_lock_view);
          mPatternLockView.setDotCount(3);
          mPatternLockView.setAspectRatioEnabled(true);
          mPatternLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS);
          mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
          mPatternLockView.setDotAnimationDuration(150);
          mPatternLockView.setPathEndAnimationDuration(100);
  //        mPatternLockView.setCorrectStateColor(0xff008000);
  //        mPatternLockView.setNormalStateColor(0xffffffff);
          mPatternLockView.setInStealthMode(false);
          mPatternLockView.setTactileFeedbackEnabled(true);
          mPatternLockView.setInputEnabled(true);
          mPatternLockView.addPatternLockListener(mPatternLockViewListener);

          RxPatternLockView.patternComplete(mPatternLockView)
                  .subscribe(new io.reactivex.functions.Consumer<PatternLockCompleteEvent>() {
                      @Override
                      public void accept(PatternLockCompleteEvent patternLockCompleteEvent) throws Exception {
                          LogUtil.error(getClass().getName(), "CallBack Complete: " + patternLockCompleteEvent.getPattern().toString());
                      }
                  });

          RxPatternLockView.patternChanges(mPatternLockView)
                  .subscribe(new Consumer<PatternLockCompoundEvent>() {
                      @Override
                      public void accept(PatternLockCompoundEvent event) throws Exception {
                          if (event.getEventType() == PatternLockCompoundEvent.EventType.PATTERN_STARTED) {
                              LogUtil.error(getClass().getName(), "CallBack Pattern drawing started");
                          } else if (event.getEventType() == PatternLockCompoundEvent.EventType.PATTERN_PROGRESS) {
                              LogUtil.error(getClass().getName(), "CallBack Pattern progress: " +
                                      PatternLockUtils.patternToString(mPatternLockView, event.getPattern()));
                          } else if (event.getEventType() == PatternLockCompoundEvent.EventType.PATTERN_COMPLETE) {
                              LogUtil.error(getClass().getName(), "CallBack Pattern complete: " +
                                      PatternLockUtils.patternToString(mPatternLockView, event.getPattern()));
                          } else if (event.getEventType() == PatternLockCompoundEvent.EventType.PATTERN_CLEARED) {
                              LogUtil.error(getClass().getName(), "CallBack Pattern has been cleared");
                          }
                      }
                  });
      }

      @Override
      public void onActive() {
          super.onActive();
      }

      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }

```


# License

Copyright 2017 aritraroy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

