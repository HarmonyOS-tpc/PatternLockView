package com.andrognito.patternlockview.listener;

/**
 * Created by aritraroy on 19/03/17.
 */

import com.andrognito.patternlockview.PatternLockView;

import java.util.List;

/**
 * The callback interface for detecting patterns entered by the user
 */
public interface PatternLockViewListener {

    /**
     * Fired when the pattern drawing has just started
     */
    void onStarted();


    void onProgress(List<PatternLockView.Dot> progressPattern);


    void onComplete(List<PatternLockView.Dot> pattern);

    /**
     * Fired when the patten has been cleared from the view
     */
    void onCleared();
}