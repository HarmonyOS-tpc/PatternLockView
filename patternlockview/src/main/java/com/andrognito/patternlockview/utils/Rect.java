/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andrognito.patternlockview.utils;

/**
 * Rect
 */
public class Rect extends ohos.agp.utils.Rect {
    /**
     * union
     *
     * @param rect Rect
     */
    public void union(Rect rect) {
        union(rect.left, rect.top, rect.right, rect.bottom);
    }

    /**
     * union
     *
     * @param left   左
     * @param top    上
     * @param right  右
     * @param bottom 下
     */
    public void union(int left, int top, int right, int bottom) {
        if ((left < right) && (top < bottom)) {
            if ((this.left < this.right) && (this.top < this.bottom)) {
                if (this.left > left) {
                    this.left = left;
                }
                if (this.top > top) {
                    this.top = top;
                }
                if (this.right < right) {
                    this.right = right;
                }
                if (this.bottom < bottom) {
                    this.bottom = bottom;
                }
            } else {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }
        }
    }


}
