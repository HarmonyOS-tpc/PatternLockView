/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andrognito.patternlockview.utils;

import ohos.agp.components.AttrSet;

/**
 * AttrUtils
 */
public class AttrUtils {
    /**
     * getIntFromAttr
     * @param attrs AttrSet
     * @param name 属性名
     * @param defaultValue 属性值
     * @return int 值
     */
    public static int getIntFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
            value = attrs.getAttr(name).get().getIntegerValue();
        }
        return value;
    }

    /**
     * getBooleanFromAttr
     * @param attrs AttrSet
     * @param name 属性名
     * @param defaultValue 属性值
     * @return boolean
     */
    public static boolean getBooleanFromAttr(AttrSet attrs, String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
            value = attrs.getAttr(name).get().getBoolValue();
        }
        return value;
    }


    /**
     * getColorFromAttr
     * @param attrs AttrSet
     * @param name 属性名
     * @param defaultValue 属性值
     * @return int
     */
    public static int getColorFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
            value = attrs.getAttr(name).get().getColorValue().getValue();
        }
        return value;
    }

    /**
     * getDimensionFromAttr
     * @param attrs AttrSet
     * @param name 属性名
     * @param defaultValue 属性值
     * @return int
     */
    public static int getDimensionFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
            value = attrs.getAttr(name).get().getDimensionValue();
        }
        return value;
    }

    /**
     * getStringFromAttr
     * @param attrs AttrSet
     * @param name 属性名
     * @param defaultValue 属性值
     * @return String
     */
    public static String getStringFromAttr(AttrSet attrs, String name, String defaultValue) {
        String value = defaultValue;
        if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
            value = attrs.getAttr(name).get().getStringValue();
        }
        return value;
    }
}
